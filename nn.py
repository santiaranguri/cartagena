import time, random, uuid
from multiprocessing import Value, Lock
from processor import Sensor, Neuron, Actuator
from queue import Queue

class NeuralNet:
    def __init__(self, env):
        self.id = uuid.uuid4()
        self.env = env
        self.processors_to_increment = Queue()
        self.sensors = {}
        self.processors = []
        self.reward = 0
        self.time_lived = 0
        self.offspring = 0
        self.age = 0

    def add_sensor(self):
        sensor = Sensor(self)
        self.processors.append(sensor)
        return sensor

    def add_actuator(self):
        actuator = Actuator(self)
        self.processors.append(actuator)
        return actuator

    def add_neuron(self):
        neuron = Neuron(self)
        self.processors.append(neuron)
        return neuron

    def remove_processor(self, processor):
        self.processors.remove(processor)

    def get_random_processor(self):
        return random.choice(self.processors)

    def add_processor_to_increment(self, processor, weight):
        self.processors_to_increment.put([processor, weight])

    def live(self, input, nn_data):
        while True:
            #print (input.qsize(), self.processors_to_increment.qsize())
            if not input.empty():
                nn_id, dimension, value = input.get()
                self.add_processor_to_increment(self.sensors[nn_id][dimension], value)

            if not self.processors_to_increment.empty():
                processor, value = self.processors_to_increment.get()
                processor.increment_state(value, nn_data)

            if input.empty() and self.processors_to_increment.empty():
                self.env.send_input(self)

            if not nn_data.am_alive():
                break

class NNData(object):
    def __init__(self):
        self.terminated = Value('b', False)
        self.lock = Lock()

    def terminate(self):
        with self.lock:
            self.terminated.value = True

    def am_alive(self):
        with self.lock:
            return not self.terminated.value
