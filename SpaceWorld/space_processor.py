import sys
sys.path.append('../')
from processor import Sensor, Neuron, Actuator

class SpaceSensor(Sensor):
    def __init__(self, nn, dimension):
        Actuator.__init__(self, nn)
        self.dimension = dimension

class SpaceActuator(Actuator):
    def __init__(self, nn, dimension, direction):
        Actuator.__init__(self, nn)
        self.dimension = dimension
        self.direction = direction

    def activate(self, nn_data):
        self.nn.space_actuator_move(self.dimension, self.direction, self.state, nn_data)
        self.state = self.initial_state
