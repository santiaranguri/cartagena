import time, random
from space_env import SpaceEnv

dimensions = {'x': 700, 'y': 700}
teams = [0]
env = SpaceEnv(dimensions, teams)

# We spawn a population
env.pop_size = 10
for _ in range(env.pop_size):
    env.create_nn()

for nn in env.nns:
    env.add_processsors_nn(nn)
    env.spawn_nn(nn)

env.draw_and_test_nns()
