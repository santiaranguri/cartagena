import sys, string, time, random
from multiprocessing import Value
sys.path.append('../')
from nn import NeuralNet, NNData
from space_processor import SpaceSensor, Neuron, SpaceActuator

POSITION_NORMALIZER = 1

class SpaceNeuralNet(NeuralNet):
    def __init__(self, env, team, x, y):
        NeuralNet.__init__(self, env)
        self.team = team
        self.x = x
        self.y = y

    def add_space_sensor(self, other_nn, dimension):
        sensor = SpaceSensor(self, dimension)
        self.processors.append(sensor)
        if other_nn.id not in self.sensors:
            self.sensors[other_nn.id] = {}
        self.sensors[other_nn.id][dimension] = sensor
        return sensor

    def add_space_actuator(self, dimension, direction):
        actuator = SpaceActuator(self, dimension, direction)
        self.processors.append(actuator)
        return actuator

    def space_actuator_move(self, dimension, direction, weight, nn_data):
        x, y = nn_data.get_pos()

        if direction == '-':
            weight *= -1

        if dimension == 'x':
            x += weight * POSITION_NORMALIZER
        elif dimension == 'y':
            y += weight * POSITION_NORMALIZER

        nn_data.set_pos([x, y])

class SpaceNNData(NNData):
    def __init__(self, x=.0, y=.0):
        NNData.__init__(self)
        self.x = Value('d', x)
        self.y = Value('d', y)

    def get_pos(self):
        with self.lock:
            return [self.x.value, self.y.value]

    def set_pos(self, pos):
        with self.lock:
            self.x.value, self.y.value = pos
