import sys, string, random, time, math
from multiprocessing import Process
sys.path.append('../')
from env import Env
from space_nn import SpaceNeuralNet, SpaceNNData
from space_gui import SpaceGUI

delta = 20 # If two agents are at a distance delta or less, one dies. (RIP)
SLEEP_TIME = .01
EPSILON = .0000001
AGE_LIMIT = 1000

class SpaceEnv(Env):
    def __init__(self, dimensions, teams):
        Env.__init__(self)
        self.dimensions = dimensions
        self.teams = teams
        self.sensor_types = ['team', 'x', 'y']
        self.center = [self.dimensions['x'] / 2, self.dimensions['y'] / 2]

    def create_nn(self, nn=None):
        team = 0 #len(self.nns) % 2
        x, y = self.center
        if nn:
            nn.x, nn.y = x, y
        else:
            nn = SpaceNeuralNet(self, team, x, y)
        nn_data = SpaceNNData(x, y)
        self.init_nn(nn, nn_data)

    def add_processsors_nn(self, nn):
        neuron = nn.add_neuron()
        for other_nn in self.nns:
            for sensor_type in self.sensor_types:
                sensor = nn.add_space_sensor(other_nn, sensor_type)
                sensor.connect_to(neuron)

        #TODO: can I accomplish the same with two actuators instead of four?
        possible_actuators = [['x', '+'], ['x', '-'], ['y', '+'], ['y', '-']]
        for dimension, direction in possible_actuators:
            actuator = nn.add_space_actuator(dimension, direction)
            neuron.connect_to(actuator)

    """We append the two dimensions and team of all others nns to the input to process. """
    def send_input(self, nn):
        for other_nn in self.nns:
            nn_data = self.nn_data[nn].get_pos() + [nn.team]
            for sensor_type, data in list(zip(self.sensor_types, nn_data)):
                sensor = (other_nn.id, sensor_type, data)
                self.input[nn].put(sensor)

    def draw_and_test_nns(self):
        space_gui = SpaceGUI(self)
        while True:
            space_gui.draw_nns()
            self.check_out_of_bounds()
            self.check_age()
            self.update_nns()
            time.sleep(SLEEP_TIME)

    def test_nns(self):
        step = 0
        while True:

            '''
            for nn in self.env.nns:
                nn.x, nn.y = self.nn_data[nn].get_pos()
                team0 = [nn for nn in self.nns if nn.team == 0]
                team1 = [nn for nn in self.nns if nn.team == 1]

            for nn0 in team0:
                for nn1 in team1:
                    # We get the 2d-cartesian distance between two nns
                    distance = math.sqrt((nn0.x - nn1.x) ** 2 + (nn0.y - nn1.y) ** 2)
                    if distance < delta:
                        nn1.reward += 1
                        self.kill_nn(nn0)
                        nn = self.get_best_nn(team0) # TODO: The killed nn could be revived
                        new_nn = self.make_offspring(nn)
                        self.send_input(new_nn)
                        break
                nn0.reward += 1

            step += 1
            if step % 5 == 0: # TODO: This can be changed to dying because of the age.
                nn = self.get_worst_nn(team1)
                self.kill_nn(nn)
                nn = self.get_best_nn(team1)
                new_nn = self.make_offspring(nn)
                self.send_input(new_nn)
            '''
            self.out_of_bounds()

    def check_out_of_bounds(self):
        for nn in self.nns:
            x, y = self.nn_data[nn].get_pos()
            if not (0 <= x <= self.dimensions['x'] and 0 <= y <= self.dimensions['y']):
                nn.reward -= 200
                self.nn_data[nn].set_pos(self.center)
                if nn.reward < 0:
                    self.terminate_nn(nn)

    def check_age(self):
        for nn in self.nns:
            if nn.age > AGE_LIMIT:
                self.terminate_nn(nn)

    def update_nns(self):
        for nn in self.nns:
            nn.reward += 1
            nn.age += 1
            #x, y = self.nn_data[nn].get_pos()
            # We get the cartesian distance between two points.
            #distance_traveled = ((nn.x - x) ** 2 + (nn.y - y) ** 2) ** (1/2)
            # Epsilon is added to avoid division by zero.
            #nn.reward = nn.time_lived / (distance_traveled + EPSILON)

    def terminate_nn(self, nn):
        team = [other_nn for other_nn in self.nns if other_nn.team == nn.team]
        best_nn = self.get_best_nn(team)
        return super(SpaceEnv, self).terminate_nn(nn, best_nn)
