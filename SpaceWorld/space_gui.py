import time, pygame

BLACK = [0, 0, 0]
RED = [255, 0, 0]
BLUE = [0, 0, 255]
WHITE = [255, 255, 255]

class SpaceGUI(object):
    def __init__(self, env):
        self.env = env
        self.size = [env.dimensions['x'], env.dimensions['y']]
        self.screen = pygame.display.set_mode(self.size)
        pygame.init()

    def draw_nns(self):
        self.screen.fill(BLACK)
        font = pygame.font.SysFont("monospace", 12)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True

        for nn in self.env.nns:
            x, y = self.env.nn_data[nn].get_pos()
            color = BLUE if nn.team == 0 else RED
            reward = str(int(nn.reward * 100) / 100.0)
            age = str(nn.age)
            label = font.render(reward + ' ' + age, 1, WHITE)
            self.screen.blit(label, (x - 13, y - 20))
            pygame.draw.circle(self.screen, color, [int(x), int(y)], 6)

        pygame.display.flip()
