import time, random
from pprint import pprint
from copy import deepcopy
from multiprocessing import Process, Queue
from queue import Queue as NormalQueue
from nn import NeuralNet
from mutator import Mutator

class Env(object):
    def __init__(self, pop_size):
        self.pop_size = pop_size
        self.nns = []
        self.dead_nns = []
        self.processes = {}
        self.input = {}
        self.nn_data = {}
        self.mutator = Mutator()

    def init_nn(self, nn, nn_data):
        ''' We init nn's input queue & nn_data, which will connect the env with the nn '''
        self.input[nn] = Queue()
        self.nn_data[nn] = nn_data
        self.nns.append(nn)

    def spawn_nn(self, nn):
        """ We let the agent live in a new process. Of all the inputs & nn_data's, we
        pass the agent only its input & nn_data. """
        p = Process(target=nn.live, args=(self.input[nn], self.nn_data[nn]))
        self.processes[nn] = p
        p.start()
        self.send_input(nn)

    def get_input(self):
        return self.input

    def get_nn_data(self):
        return self.nn_data

    def get_worst_nn(self, nns):
        return min(nns, key=lambda x: x.reward)

    def get_best_nn(self, nns):
        return max(nns, key=lambda x: x.reward)

    def terminate_nn(self, nn_to_kill, nn_to_clone):
        self.make_offspring(nn_to_clone)
        self.kill_nn(nn_to_kill)

    def kill_nn(self, nn):
        self.nn_data[nn].terminate()
        self.dead_nns.append(nn)
        self.nns.remove(nn)
        self.processes[nn].join()
        del self.processes[nn]
        del self.input[nn]
        del self.nn_data[nn]

    def make_offspring(self, nn):
        # TODO: is there a better way of doing this? I remove them because
        # otherwise I can't deepcopy the nn
        old_queue = nn.processors_to_increment
        nn.processors_to_increment = []
        nn.env = []
        new_nn = deepcopy(nn)
        nn.processors_to_increment = old_queue
        new_nn.processors_to_increment = NormalQueue()
        nn.env = self
        new_nn.env = self
        new_nn.reward = 0
        new_nn.time_lived = 0
        new_nn.offspring = 0
        new_nn.age = 0
        self.mutator.mutate(new_nn)
        self.create_nn(new_nn)
        self.spawn_nn(new_nn)
        return new_nn
