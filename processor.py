import time, uuid, math, random

INITIAL_STATE = 0
default_bias = 0.7
default_treshold = 0.9
learning_rate = 0.1
MINIMUM_STATE = -500

class Processor(object):
    def __init__(self, nn, state=INITIAL_STATE, initial_state=INITIAL_STATE, bias=default_bias, treshold=default_treshold):
        self.id = uuid.uuid4()
        self.nn = nn
        self.next = []
        self.state = state
        self.initial_state = INITIAL_STATE
        self.bias = bias
        self.treshold = treshold

    def connect_to(self, next, weight=None):
        # TODO: should we include the set of real numbers (-1, 0)?
        weight = random.uniform(0, 1) if weight == None else weight
        connection = [next, weight]
        self.next.append(connection)

    def activate(self, nn_data):
        for processor, weight in self.next:
            if processor in self.nn.processors:
                value = weight * self.activation_function(self.state)
                self.nn.add_processor_to_increment(processor, value)
            else:
                self.next.remove([processor, weight])
        self.state = self.initial_state

    """ We accumulate the input values of the processor till the activation
        function of its value is above the treshold. """
    def increment_state(self, value, nn_data):
        self.state += value
        if self.state < MINIMUM_STATE:
            self.state = MINIMUM_STATE
        if self.activation_function(self.state) > self.treshold:
            self.activate(nn_data)
            # TODO: self.learn(learning_rate, state, processor, index)

    def activation_function(self, x):
        return 1.0/(1.0 + math.e ** (-x))

    def learn(self, alpha, state, p, i):
        self.next[i][1] += alpha * p.state * (state - p.state * self.next[i][1])

class Sensor(Processor):
    pass

class Actuator(Processor):
    pass

class Neuron(Processor):
    pass
