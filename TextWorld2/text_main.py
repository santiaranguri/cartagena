import time
from text_env import TextEnv

def judger(nn):
    if nn.text == 'ab':
        nn.reward += 1
        if nn.reward > (nn.offspring + 1) * 50:
            #print (nn.offspring)
            nn.offspring += 1
            new_nn = env.make_offspring(nn)
    else:
        nn.reward -= 1
        if nn.reward < -100:
            # Bye, bye
            env.kill_nn(nn)

def callback(nn):
    env.text_sensor(nn, 'ab ')

env = TextEnv(judger, callback)

# We spawn a population of 10 agents
for _ in range(1):
    nn = env.create_text_nn()
    env.spawn_nn(nn)
    callback(nn)
