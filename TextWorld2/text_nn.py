import sys
sys.path.append('../')
from nn import NeuralNet
from text_processor import TextActuator, TextSensor

class TextNeuralNet(NeuralNet):
    def __init__(self, id, env):
        NeuralNet.__init__(self, id, env, 'TextNeuralNet')
        self.text = ''

    def add_text_sensor(self, char):
        text_sensor = TextSensor(self, char)
        self.processors.append(text_sensor)
        self.sensors[char] = text_sensor # We make a reference to the sensor so later
                                   # we can activate it through the key.
        return text_sensor

    def add_text_actuator(self, char):
        text_actuator = TextActuator(self, char)
        self.processors.append(text_actuator)
        return text_actuator
