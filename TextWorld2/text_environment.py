import sys, string
sys.path.append('../')
from env import Env
from text_nn import TextNeuralNet

class TextEnv(Env):
    def __init__(self, judger, callback):
        Env.__init__(self, judger, callback)
        self.available_chars = string.ascii_lowercase[:2] + ' ' # "ab "
        self.text = ''

    def create_text_nn(self):
        nn = self.create_nn('TextNeuralNet')
        neuron = nn.add_neuron()

        for char in self.available_chars:
            sensor = nn.add_text_sensor(char) # Char is the sensors' index
            sensor.connect_to(neuron)

        for char in self.available_chars:
            actuator = nn.add_text_actuator(char)
            neuron.connect_to(actuator)

        return nn

    def text_sensor(self, nn, text):
        for char in text:
            #sensor_to_activate = nn.sensors[char]
            input[nn.id].put(char)#sensor_to_activate)

    def text_actuator(self, nn, char):
        if char == ' ':
            self.judger(nn) # The agent finishes a word, so we judge it
            nn.text = '' # We reset the word
        else:
            nn.text += char
