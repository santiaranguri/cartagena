import random, time

class Mutator():
    def __init__(self):
        self.mutations = [self.add_neuron, self.remove_processor,
                          self.connect_two_processors, self.disconnect_two_processors]

    def mutate(self, nn):
        for i in range(int(len(nn.processors) / 5)):
            if random.randint(0, 1) == 0:
                random.choice(self.mutations)(nn)

    def add_neuron(self, nn):
        processor = nn.add_neuron()
        prev = nn.get_random_processor()
        next = nn.get_random_processor()
        prev.connect_to(processor)
        processor.connect_to(next)

    def remove_processor(self, nn):
        processor = nn.get_random_processor()
        nn.remove_processor(processor)

    def connect_two_processors(self, nn):
        processor1 = nn.get_random_processor()
        processor2 = nn.get_random_processor()
        processor1.connect_to(processor2)

    def disconnect_two_processors(self, nn):
        processor = nn.get_random_processor()
        if len(processor.next) != 0:
            processor_index = random.randint(0, len(processor.next) - 1)
            del processor.next[processor_index]
