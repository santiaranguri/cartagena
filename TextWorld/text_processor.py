import sys
sys.path.append('../')
from processor import Sensor, Neuron, Actuator

class TextSensor(Sensor):
    def __init__(self, nn, char):
        Sensor.__init__(self, nn)
        self.char = char

class TextActuator(Actuator):
    def __init__(self, nn, char):
        Actuator.__init__(self, nn)
        self.char = char

    def activate(self):
        self.state = self.initial_state
        self.nn.env.text_actuator(self.nn, self.char)
