from text_env import TextEnv

env = TextEnv(10)
for _ in range(env.pop_size):
    env.create_nn()

for nn in env.nns:
    env.add_processsors_nn(nn)
    env.spawn_nn(nn)

env.draw_and_test_nns()
