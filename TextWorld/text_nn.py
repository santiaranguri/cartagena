import sys
sys.path.append('../')
from nn import NN
from text_processor import TextSensor, Neuron, TextActuator

class TextNN(NN):
    def __init__(self, env):
        NeuralNet.__init__(self, env)

    def add_text_sensor(self, char):
        text_sensor = TextSensor(self, char)
        self.processors.append(text_sensor)
        self.sensors[char] = text_sensor # We make a reference to the sensor so later
                                         # we can activate it through the key.
        return text_sensor

    def add_text_actuator(self, char):
        text_actuator = TextActuator(self, char)
        self.processors.append(text_actuator)
        return text_actuator
