sys.path.append('../')
from env import Env
from text_nn import TextNN

class TextEnv(Env):
    def __init__(self):
        Env.__init__(self)
        self.available_chars = string.ascii_lowercase[:2] + ' ' # "ab "

    def create_nn(self):
        nn = TextNN()
        nn_data = #SpaceNNData(x, y)
        self.init_nn(nn, nn_data)
        neuron = nn.add_neuron()

        for char in self.available_chars:
            sensor = nn.add_text_sensor(char) # Char is the sensors' index
            sensor.connect_to(neuron)

        for char in self.available_chars:
            actuator = nn.add_text_actuator(char)
            neuron.connect_to(actuator)

        return nn
